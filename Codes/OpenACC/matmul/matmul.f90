module util
  implicit none
contains
  subroutine matmat(A, B, C, m, n, p)
    real(8) :: A(m, n)
    real(8) :: B(n, p)
    real(8), intent(out) :: C(m, p)
    integer, intent(in) :: m, n, p
    integer :: i, j, k
    !$acc parallel loop gang
    do j = 1, p
      call matvec(m, n, A, B(:, j), C(:, j))
    end do
  end subroutine matmat

  subroutine matvec(m, n, A, x, Ax)
    !$acc routine worker
    real(8) :: A(m, n)
    real(8) :: x(n)
    real(8), intent(out) :: Ax(m)
    integer, intent(in) :: m, n
    integer :: i
    !$acc loop worker
    do i = 1, m
      call dot_prod(m, A(i, :), x(:), Ax(i))
    end do
  end subroutine matvec

  subroutine dot_prod(n, v1, v2, res)
    !$acc routine vector
    real(8) :: v1(n)
    real(8) :: v2(n)
    real(8), intent(out) :: res
    integer, intent(in) :: n
    integer :: i
    res = 0.0d0
    !$acc loop vector reduction(+:res)
    do i = 1, n
      res = res + v1(i) * v2(i)
    end do
  end subroutine dot_prod
end module util


program main
  use util
  implicit none
  integer, parameter :: m = 10, n = 10, p = 10
  real(8) :: A(m, n)
  real(8) :: B(n, p)
  real(8) :: C(m, p)
  integer :: i, j
  do i = 1, m
    do j = 1, n
      A(i, j) = i + j
    end do
  end do

  do i = 1, n
    do j = 1, p
      B(i, j) = i - j
    end do
  end do

  !$acc enter data copyin(A, B, C)
  call matmat(A, B, C, m, n, p)
  !$acc update host(C)
  print *, C - matmul(A, B)
end program main
