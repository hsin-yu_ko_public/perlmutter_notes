#!/bin/bash
ROOT_d=$PWD
module list &> mod_stamp.`date -I`

git clone git@gitlab.com:QEF/q-e.git

cd ./q-e
./configure --with-cuda=$CUDA_HOME --with-cuda-runtime=11.0 --with-cuda-cc=80 --enable-openmp --with-scalapack=no ARCH=crayxt
make -j 16 cp pw
cd $ROOT_d
